;
let array = ['hello', 'world', 23, null, '23'];
let type = 'string';

    function filterBy (array, type) {
        let resultArray = [];
        array.forEach(function (element) {
            if (typeof element !== type) {
                resultArray.push(element);
            }
        });
        return resultArray;
    }

console.log(filterBy(array, type));
