;
const firstName = prompt('enter your first name: ', 'Kim');
const lastName = prompt('enter your last name: ', 'ChenIn');
const birthday = prompt('enter your birthday (dd.mm.yyyy): ', '08.01.1984');

function createNewUser (first_name, last_name, birthday) {
    this.firstName = first_name;
    this.lastName = last_name;
    this.getLogin = function() {
     return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    };
    this.birthday =  birthday;
    this.getAge = function () {
                let  year = Number(this.birthday.substr(6, 4)),
                     month = Number(this.birthday.substr(3, 2)) - 1,
                     day = Number(this.birthday.substr(0, 2)),
                     today = new Date(),
                     age = today.getFullYear() - year;
        if (today.getMonth() < month
                || (today.getMonth() === month
                && today.getDate() < day)) {
                age--;
            }
                return age;
        };
    this.getPassword = function () {
        return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(6, 4);
        }
    }
const newUser = new createNewUser(firstName, lastName, birthday);

console.log(createNewUser (firstName, lastName, birthday));
console.log('Login    --> ', newUser.getLogin());
console.log('Age      --> ', newUser.getAge());
console.log('Password --> ', newUser.getPassword());